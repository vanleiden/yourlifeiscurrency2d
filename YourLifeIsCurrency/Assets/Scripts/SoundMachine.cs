﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMachine : MonoBehaviour
{

    public AudioClip[] moveSounds;
    public AudioClip winSound;
    public AudioClip deathSound;

    AudioSource audioSource;

    public static SoundMachine instance;

    // Start is called before the first frame update
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        
        else if(instance != this)
        {
            Destroy(gameObject);
        }

    }



    public void PlayMoveSound()
    {
        audioSource.PlayOneShot(moveSounds[Random.Range(0,moveSounds.Length)], 1f);
    }

    public void PlayWinSound()
    {
        audioSource.PlayOneShot(winSound, 1f);
    }

    public void PlayDeathSound()
    {
        audioSource.PlayOneShot(deathSound, 1f);
    }

}
