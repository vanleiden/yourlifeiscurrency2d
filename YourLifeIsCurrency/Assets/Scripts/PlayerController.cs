﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    public int speed = 2;
    public int distance = 1;
    public int age = 0;

    public string[] agePhaseText = {"an Infant", "a Toddler", "a Teen", "an Adult", "an Elder", "dead."};

    public bool moving = false;

    public bool gameOver = false;

    public bool levelComplete = false;

    public bool fadedOut = false;

    public GameObject goal;

    public Sprite[] lifeCycle;

    public GameObject gameOverScreen;

    public GameObject winScreen;

    public GameObject obstacles;
    
    public int round = 1;

    public Text movesLeft;

    Animator anim;

    Scene scene;

    public int level = 1;


    


    // Start is called before the first frame update
    void Start()
    {
        age = 0;
        moving = false;
        gameOver = false;
        levelComplete = false;
        gameOverScreen.SetActive(false);
        winScreen.SetActive(false);
        RefreshUI(distance.ToString());
        Debug.Log("Moves left: " + distance.ToString());
        scene = SceneManager.GetActiveScene();
        fadedOut = false;

        if(scene.name == "lvl3")
        {
            anim = goal.GetComponent<Animator>();
        }

        else
        {anim = null;}

        round = 1;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!levelComplete && age < 5)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = lifeCycle[age];
        }
        
        if(gameOver == false)
        {
            RefreshUI(distance.ToString());
        }
        
        //RayCasts
        RaycastHit2D hitup = Physics2D.Raycast(transform.position, Vector2.up, 2);
        RaycastHit2D hitdown = Physics2D.Raycast(transform.position, Vector2.down, 2);
        RaycastHit2D hitright = Physics2D.Raycast(transform.position, Vector2.right, 2);
        RaycastHit2D hitleft = Physics2D.Raycast(transform.position, Vector2.left, 2);
        
        if (age == 0)
        {
            distance = 1;
            Camera.main.backgroundColor = new Color32(169,191,190,255);
        }

        if (age == 1)
        {
            distance = 2;
            Camera.main.backgroundColor = new Color32(157,182,181,255);
        }

        if (age == 2)
        {
            distance = 3;
            Camera.main.backgroundColor = new Color32(133,164,163,255);
        }

        if (age == 3)
        {
            distance = 4;
            Camera.main.backgroundColor = new Color32(106,131,130,255);
        }

        if (age == 4)
        {
            distance = 2;
            Camera.main.backgroundColor = new Color32(93,115,114,255);
        }

        if (transform.position == goal.transform.position && !moving)
        {
            Camera.main.backgroundColor = new Color32(0,146,176,255);
            StartCoroutine(YouWon());
            levelComplete = true;
        }

        if(age == 5 && gameOver == false && levelComplete == false)
        {
            Camera.main.backgroundColor = Color.black;
            distance = 0;
            StartCoroutine(GameOver());
        }
      
        if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if(hitleft.collider == null && gameOver == false && moving == false)
            {
                moving = true;
                if(scene.name == "lvl3")
                {
                    NextRound();
                }

                if(scene.name == "lvl4" && !fadedOut)
                {
                    FadeOut();
                    fadedOut = true;
                }

                StartCoroutine(WaitandMoveLeft(distance));
            }     
        }

        if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(hitright.collider == null && gameOver == false && moving == false)
            {
                moving = true;
                if(scene.name == "lvl3")
                {
                    NextRound();
                }  

                if(scene.name == "lvl4" && !fadedOut)
                {
                    FadeOut();
                    fadedOut = true;
                }

                StartCoroutine(WaitandMoveRight(distance));    
            }          
        }


        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (hitup.collider == null && gameOver == false && moving == false)
            {
                moving = true;
                if(scene.name == "lvl3")
                {
                    NextRound();
                }  

                if(scene.name == "lvl4" && !fadedOut)
                {
                    FadeOut();
                    fadedOut = true;
                }    
                StartCoroutine(WaitandMoveUp(distance));
            }
        }

        if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            if(hitdown.collider == null && gameOver == false && moving == false)
            {
                moving = true;
                if(scene.name == "lvl3")
                {
                    NextRound();
                }  

                if(scene.name == "lvl4" && !fadedOut)
                {
                    FadeOut();
                    fadedOut = true;
                }    
                
                StartCoroutine(WaitandMoveDown(distance));
            }
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(scene.name);
        }

        if (Input.GetKeyDown(KeyCode.Space) && levelComplete == true)
        {
            Debug.Log("Load lvl" + level.ToString());
            SceneManager.LoadScene("lvl" + level.ToString());
        }

        if (scene.name == "lvl3" && round == 3)
        {
            goal.transform.GetChild(2).gameObject.SetActive(true);
        }


    }



    public void GrowUp()
    {
        if(levelComplete == false && gameOver == false)
        {
            age++;
            Debug.Log("Age = " + age);
            Debug.Log("Moves left: " + distance);
        }
    }

    public void MoveLeft()
    {
        SoundMachine.instance.PlayMoveSound();
        transform.Translate(-speed,0,0);
    }

    
    public void MoveUp()
    {
        SoundMachine.instance.PlayMoveSound();
        transform.Translate(0,speed,0);
    }

    public void MoveDown()
    {
        SoundMachine.instance.PlayMoveSound();
        transform.Translate(0,-speed,0);
    }

    public void MoveRight()
    {
        SoundMachine.instance.PlayMoveSound();
        transform.Translate(speed,0,0);
    }

    public IEnumerator WaitandMoveLeft(int moveDistance)
    {
       for (int i = 0; i < moveDistance; i++)
        {
            RaycastHit2D hitleft = Physics2D.Raycast(transform.position, Vector2.left, 2);
            if (hitleft.collider == null)
            {
                int distanceLeftInUI = distance;
                MoveLeft();
                RefreshUI(distanceLeftInUI.ToString());
                distanceLeftInUI--;
            }

            else
            {break;}

            yield return new WaitForSecondsRealtime(0.2f);
        }

        GrowUp();
        moving = false;

    }

    public IEnumerator WaitandMoveUp(int moveDistance)
    {
       for (int i = 0; i < moveDistance; i++)
        {
            RaycastHit2D hitup = Physics2D.Raycast(transform.position, Vector2.up, 2);
            if (hitup.collider == null)
            {
                int distanceLeftInUI = distance;
                MoveUp();
                RefreshUI(distanceLeftInUI.ToString());
                distanceLeftInUI--;
            }

            else
            {break;}

            yield return new WaitForSecondsRealtime(0.2f);
        }

        GrowUp();
        moving = false;

    }

    public IEnumerator WaitandMoveRight(int moveDistance)
    {
       for (int i = 0; i < moveDistance; i++)
        {
            RaycastHit2D hitright = Physics2D.Raycast(transform.position, Vector2.right, 2);
            if (hitright.collider == null)
            {
                int distanceLeftInUI = distance;
                MoveRight();
                RefreshUI(distanceLeftInUI.ToString());
                distanceLeftInUI--;
            }

            else
            {break;}

            yield return new WaitForSecondsRealtime(0.2f);
        }

        GrowUp();
        moving = false;
    }

    public IEnumerator WaitandMoveDown(int moveDistance)
    {
       for (int i = 0; i < moveDistance; i++)
        {
            RaycastHit2D hitdown = Physics2D.Raycast(transform.position, Vector2.down, 2);
            if (hitdown.collider == null)
            {
                int distanceLeftInUI = distance;
                MoveDown();
                RefreshUI(distanceLeftInUI.ToString());
                distanceLeftInUI--;
            }

            else
            {break;}

            yield return new WaitForSecondsRealtime(0.2f);
        }

        GrowUp();
        moving = false;

    }

    public IEnumerator YouWon()
    {
        if (levelComplete == false)
        {
            SoundMachine.instance.PlayWinSound();
            level++;
            Debug.Log("Level complete.");
            yield return new WaitForSecondsRealtime(0.4f);
            winScreen.SetActive(true);
            Camera.main.backgroundColor = Color.black;
        }
    }

    public IEnumerator GameOver()
    {
        if (levelComplete == false && gameOver == false)
        {
            gameOver = true;
            SoundMachine.instance.PlayDeathSound();
            Debug.Log("Game over.");
            Debug.Log("Moves left: " + distance.ToString());
            RefreshUI(distance.ToString());
            gameObject.GetComponent<SpriteRenderer>().sprite = lifeCycle[5];
            yield return new WaitForSecondsRealtime(1f);
            gameOverScreen.SetActive(true);
            Camera.main.backgroundColor = Color.black;
        }
    }

    public void RefreshUI(string distanceLeft)
    {
        movesLeft.text = "You are " + agePhaseText[age] + "\r\n Distance to travel: " + distanceLeft.ToString();
    }

    public void NextRound()
    {
        anim.SetBool("round" + round, true);
        round++;
        Debug.Log("Round" + round + " active");
    }

    public void FadeOut()
    {
        if (fadedOut == false)
        {

            obstacles.SetActive(false);
            fadedOut = true;

        }
    }

}
